package com.epam.rd.restaurant.service;

import com.epam.rd.restaurant.entity.Meal;
import com.epam.rd.restaurant.entity.Order;
import org.springframework.stereotype.Service;

@Service
public class OrderService implements IOrderService{

    @Override
    public int getSummaryPrice(Order order) {
        if(order.getMeals() == null){
            return 0;
        }
        int result = 0;
        for(Meal meal : order.getMeals()){
            result += meal.getPrice() * meal.getAmount();
        }
        return result;
    }
}
