package com.epam.rd.restaurant.service;

import com.epam.rd.restaurant.entity.User;

import java.util.List;

public interface IAccountService {
    List<User> getAllUsers();
    List<User> getUserByName(String name);
    void addUser (User user);
}
