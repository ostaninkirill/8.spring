package com.epam.rd.restaurant.service;

import com.epam.rd.restaurant.entity.Order;

public interface IOrderService {
    int getSummaryPrice(Order order);
}
