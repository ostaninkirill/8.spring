package com.epam.rd.restaurant.service;

import com.epam.rd.restaurant.entity.Meal;

import java.util.List;

public interface IMenuService {
    List<Meal> getAllCategories();
    List<Meal> getAllMealbyName(String name);
    void addMeal (Meal meal);
}
