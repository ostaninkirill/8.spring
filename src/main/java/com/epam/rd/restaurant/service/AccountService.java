package com.epam.rd.restaurant.service;

import com.epam.rd.restaurant.dao.IUserDAO;
import com.epam.rd.restaurant.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService implements IAccountService {

    @Autowired
    private IUserDAO userDAO;

    @Override
    public List<User> getAllUsers() {
        return userDAO.getAll();
    }

    @Override
    public List<User> getUserByName(String name) {
        return userDAO.getUserByName(name);
    }

    @Override
    public void addUser(User user) {
        userDAO.addUser(user);
    }


}
