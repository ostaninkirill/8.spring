package com.epam.rd.restaurant.service;

import com.epam.rd.restaurant.dao.IMealDAO;
import com.epam.rd.restaurant.entity.Meal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuService implements IMenuService{

    @Autowired
    private IMealDAO mealDAO;

    @Override
    public List<Meal> getAllCategories() {
        return mealDAO.getAllMeal().stream().filter(x -> 0 != x.getAmount()).collect(Collectors.toList());
    }

    @Override
    public List<Meal> getAllMealbyName(String name) {
        return mealDAO.getAllMealbyName(name);
    }

    @Override
    public void addMeal(Meal meal) {
        mealDAO.addMeal(meal);
    }


}
