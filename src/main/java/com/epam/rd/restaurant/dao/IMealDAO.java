package com.epam.rd.restaurant.dao;

import com.epam.rd.restaurant.entity.Meal;

import java.util.List;

public interface IMealDAO {
    List<Meal> getAllMeal();
    List<Meal> getAllMealbyName(String name);
    void addMeal (Meal meal);
    void updateMeal(Meal oldMeal, Meal newMeal);
    void deleteMeal (Meal meal);
    void mealFill();
}
