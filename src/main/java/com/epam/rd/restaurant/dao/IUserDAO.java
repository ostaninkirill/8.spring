package com.epam.rd.restaurant.dao;

import com.epam.rd.restaurant.entity.User;

import java.util.List;

public interface IUserDAO {
    List<User> getAll();
    List<User> getUserByName(String name);
    void addUser (User user);
    void updateUser(User oldUser, User newUser);
    void deleteUser (User user);
    void usersFill();


}
