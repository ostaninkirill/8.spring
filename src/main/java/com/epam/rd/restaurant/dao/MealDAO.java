package com.epam.rd.restaurant.dao;

import com.epam.rd.restaurant.demoService.DemoMeal;
import com.epam.rd.restaurant.entity.Meal;
import org.springframework.stereotype.Repository;


import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MealDAO implements IMealDAO{

    private List<Meal> meals = new ArrayList<>();

    @Override
    public List<Meal> getAllMeal() {
        return meals;
    }

    @Override
    public List<Meal> getAllMealbyName(String name) {

        return meals.stream().filter(meal -> meal.getName().equals(name)).collect(Collectors.toList());
    }

    @Override
    public void addMeal(Meal meal) {
        if (meal != null) meals.add(meal);
    }

    @Override
    public void updateMeal(Meal oldMeal, Meal newMeal) {
        if (oldMeal != null && newMeal != null) {
            meals.remove(oldMeal);
            meals.add(newMeal);
        }
    }

    @Override
    public void deleteMeal(Meal meal) {
        if (meal != null) meals.remove(meal);

    }

    @PostConstruct
    public void mealFill() {
        meals.addAll(Arrays.asList(DemoMeal.meal));
    }


}
