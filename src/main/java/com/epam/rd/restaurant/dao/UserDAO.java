package com.epam.rd.restaurant.dao;

import com.epam.rd.restaurant.demoService.DemoUsers;
import com.epam.rd.restaurant.entity.User;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserDAO implements IUserDAO{

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> getAll() {
        return users;
    }

    @Override
    public List<User> getUserByName(String name) {
        return users.stream().filter(userName -> userName.getName().equals(name)).collect(Collectors.toList());
    }

    @Override
    public void addUser(User user) {
        if (user != null) users.add(user);
    }

    @Override
    public void updateUser(User oldUser, User newUser) {
        if (oldUser != null && newUser != null) {
            users.remove(oldUser);
            users.add(newUser);
        }
    }

    @Override
    public void deleteUser(User user) {
        if (user != null) users.remove(user);
    }

    @PostConstruct
    public void usersFill() {
        users.addAll(Arrays.asList(DemoUsers.user));
    }

}
