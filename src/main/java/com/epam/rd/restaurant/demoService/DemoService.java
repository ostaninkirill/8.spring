package com.epam.rd.restaurant.demoService;

import com.epam.rd.restaurant.dao.IMealDAO;
import com.epam.rd.restaurant.dao.IUserDAO;
import com.epam.rd.restaurant.entity.Meal;
import com.epam.rd.restaurant.entity.Order;
import com.epam.rd.restaurant.entity.Role;
import com.epam.rd.restaurant.entity.User;
import com.epam.rd.restaurant.service.AccountService;
import com.epam.rd.restaurant.service.IAccountService;
import com.epam.rd.restaurant.service.IMenuService;
import com.epam.rd.restaurant.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

@Service
public class DemoService {

    @Autowired
    private IOrderService orderService;

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IAccountService accountService;

    public void execute() {

        System.out.println("Вывод всех доступных пицц из меню: ");
        System.out.println(menuService.getAllCategories() + "\n");

        System.out.println("Вывод всех пользователей: ");
        System.out.println(accountService.getAllUsers() + "\n");

        System.out.println("Поиск пиццы по названию: ");
        System.out.println(menuService.getAllMealbyName("Пицца гавайская") + "\n");

        System.out.println("Поиск человека по имени: ");
        System.out.println(accountService.getUserByName("Топский Павел") + "\n");

        System.out.println("Добавление человека: ");
        accountService.addUser(new User(4, "Тропов Евгений", LocalDate.of(1987,4,2), Role.CUSTOMER));
        System.out.println(accountService.getAllUsers() + "\n");

        System.out.println("Добавление пиццы: ");
        menuService.addMeal(new Meal(4, "Пицца мясная", 550, 1000, 0));
        System.out.println(menuService.getAllCategories());



    }

}
