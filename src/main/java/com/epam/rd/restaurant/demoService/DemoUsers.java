package com.epam.rd.restaurant.demoService;

import com.epam.rd.restaurant.entity.Role;
import com.epam.rd.restaurant.entity.User;

import java.time.LocalDate;

public class DemoUsers {

    public static User[] user = new User[]{
            new User(1, "Останин Кирилл", LocalDate.of(1998,3,9), Role.ADMIN),
            new User(2, "Иванов Иван", LocalDate.of(1997,3,5), Role.CUSTOMER),
            new User(3, "Топский Павел", LocalDate.of(1977,5,1), Role.CUSTOMER)
    };
}
