package com.epam.rd.restaurant.demoService;

import com.epam.rd.restaurant.entity.Meal;

public class DemoMeal {
    public static Meal[] meal = new Meal[]{
            new Meal(1, "Пицца кантри", 380, 600, 1),
            new Meal (2, "Пицца маргарита", 300, 400, 1),
            new Meal(3, "Пицца гавайская", 350, 500, 1)
    };
}
