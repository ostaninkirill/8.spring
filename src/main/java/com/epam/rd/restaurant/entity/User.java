package com.epam.rd.restaurant.entity;

import java.time.LocalDate;
import java.util.Objects;

public class User {
    private long id;
    private String name;
    private LocalDate birthDate;
    private Role role;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User(long id, String name, LocalDate birthDate, Role role) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(name, user.name) &&
                Objects.equals(birthDate, user.birthDate) &&
                role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, role);
    }

    @Override
    public String toString() {
        return "Пользователь: " +
                "id №" + id +
                " Имя - " + name +
                " Дата рождения - " + birthDate +
                " Роль - " + role + "\n";
    }
}
