package com.epam.rd.restaurant.entity;

public enum Role {
    CUSTOMER, ADMIN;
}
