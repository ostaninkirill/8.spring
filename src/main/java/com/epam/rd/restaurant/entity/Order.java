package com.epam.rd.restaurant.entity;

import java.time.LocalDateTime;
import java.util.List;

public class Order {
    private long id;
    private User client;
    private LocalDateTime timeOrder;
    private double price;
    private List<Meal> meals;
    private int checkAccepted;
    private long idClient;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
        this.idClient = client.getId();
    }

    public LocalDateTime getTimeOrder() {
        return timeOrder;
    }

    public void setTimeOrder(LocalDateTime timeOrder) {
        this.timeOrder = timeOrder;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public int getCheckAccepted() {
        return checkAccepted;
    }

    public void setCheckAccepted(int checkAccepted) {
        this.checkAccepted = checkAccepted;
    }

    public long getIdClient() {
        return idClient;
    }

    public void setIdClient(long idClient) {
        this.idClient = idClient;
    }


    public Order(long id, User client, LocalDateTime timeOrder, double price, List<Meal> meals, int checkAccepted, long idClient) {
        this.id = id;
        this.client = client;
        this.timeOrder = timeOrder;
        this.price = price;
        this.meals = meals;
        this.checkAccepted = checkAccepted;
        this.idClient = idClient;
    }
}
