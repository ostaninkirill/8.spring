package com.epam.rd.restaurant.entity;

import java.util.Objects;

public class Meal {
    private long id;
    private String name;
    private int price;
    private int weight;
    private int amount;


    public Meal(long id, String name, int price, int weight, int amount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.weight = weight;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meal meal = (Meal) o;
        return id == meal.id &&
                price == meal.price &&
                weight == meal.weight &&
                amount == meal.amount &&
                Objects.equals(name, meal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, weight, amount);
    }

    @Override
    public String toString() {
        return
                "id#" + id +
                ", Название -  " + name +
                ", Цена -  " + price +
                ", Вес - " + weight +
                ", Количество в наличии - " + amount + "\n";
    }
}
