package com.epam.rd.restaurant;

import com.epam.rd.restaurant.demoService.DemoMeal;
import com.epam.rd.restaurant.demoService.DemoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class RestaurantApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(RestaurantApplication.class, args);

		DemoService demo = ctx.getBean(DemoService.class);
		demo.execute();
	}

}

